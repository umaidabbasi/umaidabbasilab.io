# About Me

Hi my name's Umaid and I'm a Platform Developer based in Ottawa. I've always enjoyed developing software and 
learning about new technologies to better my craft. I enjoy working with open source projects and use a variety of OSS in my home for home automation. 

Besides technology my passions/interests include rock climbing, cycling, cooking, coffee and spending time with my dog.

# Current role
### [WealthSimple](https://www.wealthsimple.com)
I'm currently a Infrastructure Platform Engineer at Wealthsimple. 

* Build opinionated Terraform modules that are used by developers to bring up self serve infrastructure in AWS.
* Write tooling in bash and Python to automate tasks.
* Migrated various infrastructure developed in CloudFormation to Terraform, seamlessly 
* Provide guidance to developers to help containerize their Apps/Services to run on our kubernetes clusters
* Currently migrating from [Nomad](https://www.nomadproject.io/) to [EKS](https://aws.amazon.com/eks/)
 

# Past Roles

### [Shopify](https://www.shopify.ca)

* Built and maintained CI/CD pipelines on buildkite that ran code linters, unit tests, intergration tests, and finally deployed to the Google Cloud platform. 
* Built Python based RESTful APIs using the [FastAPI framework](https://fastapi.tiangolo.com)
* Contributed features to various Ruby on Rails Applications within Shopify as well as maintaining external dependencies.

### [RockPort Networks](https://rockportnetworks.com)
DevOps Engineer

* Manged Jenkins Servers, integrated them into our test infrastructure and eith SCM so builds trigger on PRs.
* Contributing to various repositories
* Release Management and branching for releases. 
* Running code analysis using [BackDuck](https://www.blackducksoftware.com)
* Moving our testing infrastructure to infrastructure as code using Python + Ansible and terraform. 


### [Ciena](https://www.ciena.ca)
At Ciena I was an Automation and System Integration Engineer. 
I a helped to build a complete devops pipeline to enable us to automate the deployment of VNFs ( Virtual Network Functions) on the edge of modern networks. This allowed our in house VNF experts to quickly deploy a customer VNF design for testing and troubleshooting issues. This was done using Gitlab CI/CD. 

### [Ford Motor](https://www.ford.ca)
Shifting from telecommunications to infotainment systems, I was a automation test developer working for Ford Motor Company under the connected vehicles division. Where I created test cases to verify Ford's implementation of [gRPC](https://grpc.io) and a proprietary token solution based on [OAuth 2.0](https://oauth.net/2/) , once created I automated these tests using the [Slash Framework](https://slash.readthedocs.io/en/master/) and Python.  


### [Nokia](https://www.nokia.com)
I was a Automation Engineer at Nokia where I primarily used Python and [RobotFramework](https://robotframework.org/) to Automate Customer Lab testing for [Nuage Networks](https://www.nuagenetworks.net/) and Nokia's Packet Core products. 


# Technical skills

- Python, including various frameworks such as [Flask](https://palletsprojects.com/p/flask/)
- Proficient in cloud providers such as AWS
- Experience in building a kubernetes
- Proficient IaC such as Terraform 
- Strong understanding of RESTful APIs
- Proficient in Linux systems, including automated building/deployment of containers
- Various Virtualization technologies such as OpenStack/KVM 
- Strong understanding of Network Technologies and various Protocols  
- Proficient in CI/CD, including setting up a CI/CD from scratch. This webpage is built using Gitlab CI/CD 

# Projects 

- [Algorithmic dashboards](https://gitlab.com/umaidabbasi/unsupervised-learning)
    - Participated In a Nokia Hackathon to build a AI driven dashboard. 
    - Based on what other users view, the application will recommend popular reports based on your user role.
    - My role was to build the API using [Flask](https://palletsprojects.com/p/flask/) to connect the backend and front end.
- [Buzzer Redirect tool](https://gitlab.com/umaidabbasi/BuildingBuzzerRedirect)
    - Simple script that runs in the cloud, uses [Twilio](https://www.twilio.com) to redirect my buzzer to my cell phone as my building does not support long distance calls. 

# Contact Me

[Linkedin](https://linkedin.com/in/umaid-abbasi)

